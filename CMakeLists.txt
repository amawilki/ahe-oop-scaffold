cmake_minimum_required(VERSION 3.14)
project(cpp_project_scaffold)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -std=c++17 -Wall -pedantic -Wextra -Werror -pthread -O2") # Enable c++17 standard

include_directories(include)

add_executable(cpp_project_scaffold
        src/main.cpp
        src/View.cpp
        include/View.h
        )
